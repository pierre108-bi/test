Feature: DataSet operations
	@automated-rest
	Scenario: Copy DataSet without submit
		Given exist approved DataSet
		And user with profile "fdm" is logged
		When DataSet copying start
		And select one existing DataSet version of "0406_BG"
		And the user fill up all information for destination Database
			|PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
			|T| hadoop| qa | bi-as-datalake-qa-us | dvds_master_ca_current |
		And copy of DataSet is saved
		Then new DataSet is visible in the list
		And new DataSet has Version new

	@automated-rest
	Scenario: Copy DataSet with submit
		Given exist approved DataSet
		And user with profile "fdm" is logged
		When DataSet copying start
		And select one existing DataSet version of "0406_BG"
		And the user fill up all information for destination Database
			|PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
			|T| hadoop| qa | bi-as-datalake-qa-us | dvds_master_ca_current |
		And copy of DataSet is saved and submitted
		Then new DataSet is visible in the list
		And new DataSet has Version approved

	@automated-rest
	Scenario: Copy DataSet without submit
		Given exist approved DataSet
		And user with profile "fdm" is logged
		When DataSet copying start
		And select one existing DataSet version of "0406_BG"
		And the user fill up all information for destination Database
			|PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
			|T| oracle| prod | bi-as-odldsa-prod | test_dlms |
		And copy of DataSet is saved
		Then new DataSet is visible in the list
		And new DataSet has Version new

	@automated-rest
	Scenario: Copy DataSet with submit
		Given exist approved DataSet
		And user with profile "fdm" is logged
		When DataSet copying start
		And select one existing DataSet version of "0406_BG"
		And the user fill up all information for destination Database
			|PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
			|T| oracle| prod | bi-as-odldsa-prod | test_dlms |
		And copy of DataSet is saved and submitted
		Then new DataSet is visible in the list
		And new DataSet has Version approved


	@automated-rest @positive
	Scenario: Copy DataSet with PARQUET format into Hive Database without submit
		Given exist approved DataSet
		And user with profile "fdm" is logged
		When DataSet copying start
		And select one existing DataSet version of "0406_Customer_Attribute_BI__c"
		And the user fill up all information for destination Database
			|PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
			|T| hadoop| qa | bi-as-datalake-qa-us | DELETETEST6_MASTER_CA_CURRENT |
		And copy of DataSet is saved
		Then new DataSet is visible in the list
		And new DataSet has Version new

	@automated-rest @positive
	Scenario: Copy DataSet with PARQUET format into Hive Database with submit
		Given exist approved DataSet
		And user with profile "fdm" is logged
		When DataSet copying start
		And select one existing DataSet version of "0406_Customer_Attribute_BI__c"
		And the user fill up all information for destination Database
			|PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
			|T| hadoop| qa | bi-as-datalake-qa-us | DELETETEST6_MASTER_CA_CURRENT |
		And copy of DataSet is saved and submitted
		Then new DataSet is visible in the list
		And new DataSet has Version approved

	@automated-rest @positive
	Scenario: Copy DataSet with PARQUET format into Hive Database with submit
		Given exist approved DataSet
		And user with profile "fdm" is logged
		When DataSet copying start
		And select one existing DataSet version of "test_PETER"
		And the user fill up all information for destination Database
			|PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
			|T| hadoop| qa | bi-as-datalake-qa-us | DELETETEST6_MASTER_CA_CURRENT |
		And copy of DataSet is saved and submitted
		Then new DataSet is visible in the list
		And new DataSet has Version approved
		And Hive table is created with corresponding name
		And Hive table has correct structure


	@automated-rest @positive
	Scenario: Create new DataSet
		Given exist approved DataSet


	@automated-rest @positive
	Scenario: Modify existing DataSet
		Given exist approved DataSet


	@automated-rest @positive
	Scenario: Adding new Version to  DataSet
		Given exist approved DataSet
