Feature: Solution operations
	Scenario: Create new Solution Security model
		Given exist

	Scenario: Create new Solution with security model
		Given exist

    Scenario: Create Database with Solution and security model
		Given exist

	Scenario: Delete used directory in security model
		Given exist security model with more directories
		When the user delete directory which is used
		Then error message appeared

	Scenario: Delete not used directory in security model
		Given exist security model with more directories
		When the user delete directory which is not used
		Then directory is deleted
